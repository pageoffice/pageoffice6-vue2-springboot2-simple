import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import service from './utils/request'; // 引入request模块


Vue.config.productionTip = false
Vue.prototype.$http = service; // 全局注册axios实例

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')