import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    // 其他路由配置项...
    // 下面添加DocView.vue的路由
    path: '/showDoc',
    name: 'doc',
    component: () =>
        import ('../views/DocView.vue')
}

]

const router = new VueRouter({
	mode:'history',
	routes
})

export default router
