package com.test.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.test.util.GetDirPathUtil;
import org.springframework.web.bind.annotation.*;
import com.zhuozhengsoft.pageoffice.*;

@RestController
@RequestMapping(value = "/doc")
public class DocumentController {
	//获取doc目录的磁盘路径
	private String dir = GetDirPathUtil.getDirPath() + "static/";

	@RequestMapping(value="/openFile")
	public String openFile(HttpServletRequest request,int file_id,String file_name)  {
		//file_id和file_name是为了展示如何使用参数，我们这里只用到了file_name
		PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
		//webOpen的第一个参数支持能够输出下载文件的Url相对地址或者文件在服务器上的磁盘路径两种方式
		//查看详细，请在"https://www.pageoffice.cn/"搜索“PageOffice属性或方法中涉及到的URL路径或磁盘路径的说明”
		poCtrl.webOpen("file://"+dir+file_name, OpenModeType.docNormalEdit, "张三");
		return poCtrl.getHtmlCode();//必须
	}

	@RequestMapping("/saveFile")
	public void saveFile(HttpServletRequest request, HttpServletResponse response,int file_id,String file_name)  {
		//file_id和file_name是为了展示如何使用参数，我们这里只用到了file_name
		FileSaver fs = new FileSaver(request, response);
		fs.saveToFile(dir + file_name);
		fs.close();
	}
}

