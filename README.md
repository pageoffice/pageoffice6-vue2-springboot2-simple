# pageoffice6-vue2-springboot2-simple

**当前版本：6.5.0.1**

### 一、简介

​       pageoffice6-vue2-springboot2-simple项目演示了在前端Vue框架和后端Springboot框架的结合下如何使用PageOffice V6.0产品，此项目是一个最简单的demo项目。

### 二、项目环境要求

- 前端Vue项目（vue-front）:Node.js10及以上版本
- 后端Springboot项目（springboot-back）：Intelij IDEA,jdk1.8及以上版本

### 三、项目运行准备

  1. 在后端项目所在磁盘上新建一个pageoffice系统文件夹，例如：D:/pageoffice（此文件夹将用来存放PageOffice注册后生成的授权文件 “license.lic 或 license_gc.lic” 和 PageOffice客户端程序）。

 2. 下载PageOffice客户端程序



- 说明：

  PageOffice按客户端电脑操作系统不同分为两款产品。 **PageOffice Windows版** 和  **PageOffice 国产版** 。

  **PageOffice Windows版** ：支持Win7/Win8/Win10/Win11。

  **PageOffice 国产版** ：支持信创系统，支持银河麒麟V10和统信UOS，支持X86（intel、兆芯等）、ARM（飞腾、鲲鹏等）、龙芯（Loogarch64）、龙芯（MIPS）芯片架构。

  如果您的用户客户端电脑都是Windows系统，只需要下载windows版客户端。

  如果您的用户客户端电脑都是国产系统，只需要下载对应cpu芯片的国产版客户端。

  如果您的用户客户端电脑既有Windows系统又有国产系统，那么Windows安装包和国产版安装包（根据操作系统和cpu芯片选择）都需要下载。

  _注意：如果您需要支持WinXP，您只能使用PageOffice5.0及早期老版本。 如果您需要支持申威架构，您目前只能使用PageOffice国产版5.0_  。

- 下载地址：

  **注意**：下面地址随着时间推移可能会失效，请下载到本地做好备份。或者直接点击 [pageoffice6-client](https://gitee.com/pageoffice/pageoffice6-client/releases) 获取pageoffice最新版本。

   Windows版客户端下载地址：https://gitee.com/pageoffice/pageoffice6-client/releases/download/v6.5.0.1/posetup_6.5.0.1.exe

   国产版客户端下载地址：

  **统信UOS系统：**

     X86芯片(Intel、兆芯)：https://gitee.com/pageoffice/pageoffice6-client/releases/download/v6.5.0.1/com.zhuozhengsoft.pageoffice_6.5.0.1_amd64_uos.deb
  
    ARM芯片(鲲鹏、飞腾)：https://gitee.com/pageoffice/pageoffice6-client/releases/download/v6.5.0.1/com.zhuozhengsoft.pageoffice_6.5.0.1_arm64_uos.deb
  
  **银河麒麟V10系统：**
  
     X86芯片(Intel、兆芯)：https://gitee.com/pageoffice/pageoffice6-client/releases/download/v6.5.0.1/com.zhuozhengsoft.pageoffice_6.5.0.1_amd64_kylin.deb
  
    ARM芯片(鲲鹏、飞腾)：https://gitee.com/pageoffice/pageoffice6-client/releases/download/v6.5.0.1/com.zhuozhengsoft.pageoffice_6.5.0.1_arm64_kylin.deb

3.拷贝下载到的客户端程序到上面创建的 pageoffice 系统文件夹下。

**注意：客户端程序的版本必须和服务器端pom文件里面依赖pageoffice的版本号 6.X.X.X-javax保持一致。**

### 四、项目运行步骤

- #### 后端Springboot项目（springboot-back）

1. 打开application.properties文件，将posyspath变量的值配置成您上一步新建的PageOffice系统文件夹  （例如：D:/pageoffice）。
2. 运行项目：点击运行按钮即可。

> 注意：如果后端Springboot项目的8081端口已经被占用，后端项目更换其他端口后，请记得将前端Vue项目vue-front/vite.config.js文件中proxy对象中的target指向的地址改成更改后的后端项目的端口。

- #### 前端Vue项目（vue-front）

1. **npm install** ：安装依赖
2. **npm run serve** ：运行启动

### 五、PageOffice序列号

在首次运行项目中点击 “在线打开文档” ，PageOffice会弹出注册对话框。如果您需要使用PageOffice Windows版，请在windows 客户端电脑上访问pageoffice页面，用windows 版序列号进行注册。如果您需要使用PageOffice国产版，请在国产操作系统的客户端电脑上访问pageoffice页面，用国产版序列号进行注册。

**PageOffice windows 版试用序列号：**

PageOfficeV6.0标准版试用序列号：A7VHK-HDTK-338U-NARCV

PageOfficeV6.0专业版试用序列号：6VD6L-3MJL-DASM-YD9B5

**PageOffice 国产版试用序列号：**

PageOfficeV6.0国产版试用序列号：GC-8H-693A-Z1IA-R62SJ

> 问：PageOffice浏览器窗口中如何调试？
>
> 答：PageOffice浏览器（POBrowser窗口）中，在网页空隙中右键"网页调试工具"，即可出现"PageOffice Devtools"工具窗口进行调试。

### 六、联系我们

   卓正官网：[https://www.zhuozhengsoft.com](http://www.zhuozhengsoft.com)

   PageOffice开发者中心：https://www.pageoffice.cn/

   视频演示地址：

   - Windows版：https://www.bilibili.com/video/BV1M34y1A7qL

   - 国产版：https://www.bilibili.com/video/BV1eM4m167Lc/

   联系电话：400-6600-770  

   QQ: 800038353